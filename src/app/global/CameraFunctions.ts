import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Injectable } from "@angular/core";
//import { log } from 'console';

@Injectable()
export class CamaraFunctions{
  [x: string]: any;

    image:string = null;

    constructor(private camera:Camera) {}

  /*  createDirectory ( rootDirEntry ) 
    { rootDirEntry . getDirectory ( 'NewDirInRoot' , { create : true }, function ( dirEntry ) 
    { dirEntry . getDirectory ( 'images' , { create : true }, function ( subDirEntry ) 
    { createFile ( subDirEntry , "fileInNewSubDir.txt" ); }, onErrorGetDir ); }, onErrorGetDir ); } */
    
    tomarFoto(){
        let options: CameraOptions = {
          destinationType: this.camera.DestinationType.DATA_URL,
          targetWidth: 1200,  // medidas 4
          targetHeight: 1800, // x 6
          quality: 100
        }
        this.camera.getPicture( options )
        .then(imageData => {
          this.image = `data:image/jpeg;base64,${imageData}`;
        })
        .catch(error =>{
          console.error( error );
        });
      
     
          
      
    }

}