import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./pages/list/list.module').then(m => m.ListPageModule)
  },
  { 
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule) 
  },
  { 
    path: 'carpetas',
    loadChildren: () => import('./pages/carpetas/carpetas.module').then(m => m.CarpetasPageModule) 
  },
  { path: 'expo', loadChildren: './pages/expo/expo.module#ExpoPageModule' },
  { path: 'articulo', loadChildren: './pages/articulo/articulo.module#ArticuloPageModule' },
  { path: 'expo-nueva', loadChildren: './pages/expo-nueva/expo-nueva.module#ExpoNuevaPageModule' },
  


 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
