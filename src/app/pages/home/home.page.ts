import { Component } from '@angular/core';
import { CamaraFunctions } from '../../global/CameraFunctions';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';
//import { File } from '@ionic-native/file';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private camara:CamaraFunctions, private modalController: ModalController) {}  
  async nuevaFoto(){
    const modal= await this.modalController.create({
      component : ModalPage
    });
    this.camara.tomarFoto();
    modal.present();
  }

 /*crearCarpeta(){
    this.file.createDir(this.file.dataDirectory, 'STYLOS', true).then(response => {
      console.log('Directory create'+response)})
  };*/

}
