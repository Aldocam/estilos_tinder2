import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpoNuevaPage } from './expo-nueva.page';

describe('ExpoNuevaPage', () => {
  let component: ExpoNuevaPage;
  let fixture: ComponentFixture<ExpoNuevaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpoNuevaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpoNuevaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
