import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExpoNuevaPage } from './expo-nueva.page';

const routes: Routes = [
  {
    path: '',
    component: ExpoNuevaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExpoNuevaPage]
})
export class ExpoNuevaPageModule {}
