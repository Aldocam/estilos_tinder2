import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArticuloNuevoPage } from './articulo-nuevo.page';

const routes: Routes = [
  {
    path: '',
    component: ArticuloNuevoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArticuloNuevoPage]
})
export class ArticuloNuevoPageModule {}
