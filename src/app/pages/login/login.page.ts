import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

 
  constructor(private route: Router, private alertCtrl: AlertController) { }
  ngOnInit() {
  }
  async validar(){
    let alert = this.alertCtrl.create({
      header: 'Inicio Correcto',
      buttons: ['ok']
    });
    (await alert).present();
    this.route.navigate(['/carpetas']);
  }
}
