import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {
 // searchQuery: string = ''; //Busqueda de colores
  //items;  //constante para la lista
  
  Color;
  Modelo;
 
  constructor( public alertController: AlertController, public modalController: ModalController, public route: Router, private alertCtrl: AlertController) {    
  //iniciar la lista en el contructor   this.initializeItems();
    } 
  closeModal(){
    this.modalController.dismiss(); 

  }
  async abrirExpo(){
  
    let alert = this.alertCtrl.create({
      header: 'Agregados',
      subHeader: this.Modelo,
      message: this.Color,
      buttons: ['ok']
    });
    (await alert).present();
    this.route.navigate(['/expo']); //Cambio de ruta
  }
  ngOnInit() {
  }

   //función para ocultar y mostrar colores
  /*public ocultar1: boolean = false;
  accion1(){
  this.ocultar1 = !this.ocultar1;
  }


  async otro() {  //función del botón "otro", para agregar un color no existente en la lista por medio de un alert
    const alert = await this.alertController.create({
      header: 'Escriba el color',
      inputs: [
        {
          name: 'color',
          type: 'text',
          placeholder: 'Escriba aquí'
        }
      ],
      buttons: [  {
        text: 'Agregar',
        handler: () => {
          console.log('Confirm Ok');
        }
      }, {     
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
        }
      }]
    });

    await alert.present();
  }*/
/*
  // Funcipon para obtener los colores de la lista
  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    
  }
  buttonClick(){    //función al dar click en un color
    alert(this.items.item);
  }


  initializeItems() {   //función de la lista de todos los colores
    this.items = [
'NEGRO',
'BLANCO',
'VINO',
'HUESO',
'PLATA',
'BEIGE',
'AMARILLO',
'NARANJA',
'GRIS',
'ROSA',
'LILA',
'CAFE',
'AZUL',
'MARFIL',
'COCOA',
'ROJO',
'MAPLE',
'CANARIO',
'A.MARINO',
'ACERO',
'ALMENDRA',
'ALUMINIO',
'AMARETO',
'AQUA',
'ARENA',
'ATLA',
'BARBI',
'BARBIE',
'BLACK',
'BLANCA',
'BLOOD',
'BLUE',
'BOND',
'BRANDY',
'BRIAR',
'BRONCE',
'BROWN',
'BROWN/MORO',
'BURGUNDY',
'CACAO',
'CACTUS',
'CAMEL',
'CANELA',
'CAOBA',
'CAPUCHINO',
'CARAMELO',
'CARBON',
'CARDENAL',
'CARTON',
'CASTANO',
'CASTANO MKP',
'CELESTE',
'CEMENTO',
'CEREZA',
'CESPED',
'CHEDRON',
'CHEDRON OBSOLETO',
'CHERRY',
'CHOCOBIT',
'CHOCOLATE',
    ];
  }*/
}
