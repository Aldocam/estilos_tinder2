import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpoPage } from './expo.page';

describe('ExpoPage', () => {
  let component: ExpoPage;
  let fixture: ComponentFixture<ExpoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
