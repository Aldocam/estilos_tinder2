import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { CamaraFunctions } from '../../global/CameraFunctions';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-expo',
  templateUrl: './expo.page.html',
  styleUrls: ['./expo.page.scss'],
})
export class ExpoPage{

  constructor(private route: Router, private camara:CamaraFunctions, private modalController: ModalController) { }
  async nuevaFoto(){
    const modal= await this.modalController.create({
      component : ModalPage
    });
    this.camara.tomarFoto();    //importar función para tomar foto
    modal.present();
  }
 
VerImagen(){
  this.route.navigate(['/articulo']);
}
}
