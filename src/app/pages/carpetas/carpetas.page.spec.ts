import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarpetasPage } from './carpetas.page';

describe('CarpetasPage', () => {
  let component: CarpetasPage;
  let fixture: ComponentFixture<CarpetasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarpetasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarpetasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
